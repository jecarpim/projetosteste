package fatec.poo.aula5;

public class FuncionarioHorista extends Funcionario{
	  private double ValHorTrab;
	  private int QtdeHorTrab;
		
	  public FuncionarioHorista(int r,String n,String dta,double vht)  {
	     super(r, n, dta); // chamada ao m�todo construtor da superclasse
	     ValHorTrab = vht;
	  }

	  public void apontarHoras(int qht)   {
	       QtdeHorTrab=qht;
	  }  

	   public double calcSalBruto() {
	       double SalBruto;
	       SalBruto=ValHorTrab *  QtdeHorTrab;
	       return(SalBruto);
	   }
	   
	   public double calcGratificacao(){
		   return(0.075 * calcSalBruto());
	   }
	   
	   public double calcSalLiquido() {
	          double SalLiq;
	          SalLiq=super.calcSalLiquido()+ calcGratificacao();
	          return(SalLiq);
	    }	

}








