package fatec.poo.aula5;

public class FuncionarioMensalista extends Funcionario{
	private double ValSalMin;
	private double NumSalMin;

	public FuncionarioMensalista(int r,
			                     String n,
			                     String dta,
			                     double nsm) {
		//chamada ao m�todo construtor da superclasse
		super(r, n, dta); 
		NumSalMin = nsm;
	}

	public void apontarValSalMin(double vsm) {
		ValSalMin=vsm;
	}

	public double calcSalBruto() {
		double SalBruto;
		SalBruto = NumSalMin * ValSalMin;
		return (SalBruto);
	}
}
