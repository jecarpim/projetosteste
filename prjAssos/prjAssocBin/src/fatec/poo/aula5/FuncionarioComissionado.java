package fatec.poo.aula5;

public class FuncionarioComissionado extends Funcionario {
	private double SalBase;
	private double TaxaComissao; 
	private double TotalVendas;
	
	public FuncionarioComissionado(int Registro,
			                       String Nome, 
			                       String DtAdmissao,
			                       double TaxaComissao){
		super(Registro, Nome, DtAdmissao);
		this.TaxaComissao = TaxaComissao; //porcentagem
	}
	public void setSalBase(double salBase){
		SalBase = salBase;
	}
	public double getsalBase(){
		return(SalBase);
	}	
	public double getTaxaComissao() {
		return TaxaComissao;
	}	
	public void addVendas(double venda){
		TotalVendas += venda;
	}
	
	public double calcSalBruto(){
		double SalBruto;
		SalBruto = TotalVendas * TaxaComissao/100 + SalBase;
		return(SalBruto);
	}
	
	public double calcGratificacao(){
		double Gratificacao = 0;		
		
		if(TotalVendas > 5000 && TotalVendas <= 10000){
			Gratificacao = 0.025 * calcSalBruto();
		}
		else
		  if (TotalVendas > 10000){
			Gratificacao = 0.035 * calcSalBruto();
		 }
		return(Gratificacao);
	}
	
	public double calcSalLiquido() {
		double SalLiq;
		SalLiq = super.calcSalLiquido()+ calcGratificacao();
		return (SalLiq);
	}
}
