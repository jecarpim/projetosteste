package fatec.poo.aula5;

public class Departamento {
    private String sigla;
    private String descricao;
	private Funcionario[] funcionarios;
	private int qtdeFunc=0;
	
    public Departamento(String sigla, String descricao) {
		this.sigla = sigla;
		this.descricao = descricao;
		funcionarios = new Funcionario[5];
	}
    public void addFuncionario(Funcionario f){
    	funcionarios[qtdeFunc++]=f;
    }
	public String getSigla() {
		return sigla;
	}

	public String getDescricao() {
		return descricao;
	}
	
	public void exibirFuncionarios() {
		
		
		
	}
    
}
