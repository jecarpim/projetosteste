package fatec.poo.aula5;

public class Aplic {

	public static void main(String[] args) {
		FuncionarioHorista funchor=new FuncionarioHorista(1010,
                                                          "Pedro Silveira",
                                                          "14/05/1978",
                                                          15.80); 
        funchor.setCargo("Programador");

        FuncionarioMensalista funcmen=new FuncionarioMensalista(2020,
        														"Ana Beatriz",
        														"23/09/2007",
        														2); 
        funcmen.setCargo("Auxiliar Adminisrativo");

        
        Departamento dep=new Departamento("CMP", "Compras");
        
        Projeto proj=new Projeto(1010, "Seguran�a no Trabalho");
        
        proj.setDataInicio("07/04/2014");
        proj.setDataTermino("11/06/2014");
        
        //Estabelecendo a associa��o bin�ria 1..1 entre um
        //objeto da classe Funcionario com um objeto da 
        //classe Departamento
        funchor.setDepartamento(dep);
        funcmen.setDepartamento(dep);
        System.out.println("O funcionario " + funchor.getNome() +
        		           " trabalha no departamento " + 
        		           funchor.getDepartamento().getDescricao());
        
        System.out.println("O funcionario " + funcmen.getNome() +
		           " trabalha no departamento " + 
		           funcmen.getDepartamento().getDescricao());
        
      //Estabelecendo a associa��o bin�ria 1..* entre um
      //objeto da classe Departamento com um ou mais objetos da 
      //classe Funcionario
      dep.addFuncionario(funchor);
      dep.addFuncionario(funcmen);
      
      
      dep.exibirFuncionarios();

      
      //Estabelecendo a associa��o bin�ria 1..1 entre um
      //objeto da classe Funcionario com um objeto da 
      //classe Projeto
      funchor.setProjeto(proj);
      funcmen.setProjeto(proj);
      
      //Estabelecendo a associa��o bin�ria 1..* entre um
      //objeto da classe Projeto com um ou mais objetos da 
      //classe Funcionario
      proj.addFuncionario(funchor);
      proj.addFuncionario(funcmen);
      
      
      proj.exibirFuncionarios();
	}

}
