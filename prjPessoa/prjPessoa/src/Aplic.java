
public class Aplic {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
            
            Pessoa Cadastro[] =  new Pessoa[5];
            Cadastro[0] = new PessoaFisca("Jo�o", 2012, "123456");
            Cadastro[1] = new PessoaJuridica("Elson", 2012, "5673456");
            
            Cadastro[0].addCompras(12100);
            ((PessoaFisca)Cadastro[0]).setBase(100);
            
            Cadastro[1].addCompras(100);
            ((PessoaJuridica)Cadastro[1]).setTaxaIncentivo(10);
            
            
            
            for(int x=0; x < Cadastro.length; x ++ ){
               System.out.println("\nNome: " + Cadastro[x].getNome());
               System.out.println("Ano Inscr: " +Cadastro[x].getAnoInscricao());
               System.out.println("Bonus: " + Cadastro[x].calcBonus(2014));
               if(Cadastro[x] instanceof PessoaFisca){
                    System.out.println("CPF: " + ((PessoaFisca)Cadastro[x]).getCPF());
               }
               if(Cadastro[x] instanceof PessoaJuridica){
                    System.out.println("CGC: " + ((PessoaJuridica)Cadastro[x]).getCGC());
               }
            }
            
            System.out.println("");
       }
            
	}
        
