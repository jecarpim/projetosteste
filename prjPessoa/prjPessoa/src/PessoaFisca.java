import java.sql.Date;
import java.util.Calendar;


public class PessoaFisca extends Pessoa{

	private String CPF;
	private double Base;
	
	public PessoaFisca(String nome, int ano, String cpf) {
		super(nome, ano);
		CPF = cpf;
	}
	
	
	
	public double getBase() {
		return Base;
	}



	public void setBase(double base) {
		Base = base;
	}



	public String getCPF() {
		return CPF;
	}



	@Override
	public double calcBonus(int ano) {
		double bonus=0;
	
		if(getTotalCompras()> 12000){
			bonus = ((ano - getAnoInscricao()) * Base);
		}
		
		return bonus;
	}



	


}
