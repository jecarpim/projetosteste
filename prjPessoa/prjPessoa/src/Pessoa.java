import java.io.ObjectInputStream.GetField;


public abstract class Pessoa {
	private String Nome;
	private int AnoInscricao;
	private double TotalCompras=0;
	
	public Pessoa(String nome,int ano)
	{
		Nome = nome;
		AnoInscricao = ano;
	}
	abstract public double calcBonus(int ano); 
	
	public void addCompras(double compras){
		TotalCompras = TotalCompras + compras;
	}

	public String getNome() {
		return Nome;
	}

	public int getAnoInscricao() {
		return AnoInscricao;
	}

	public double getTotalCompras() {
		return TotalCompras;
	}
	
	
	
}

