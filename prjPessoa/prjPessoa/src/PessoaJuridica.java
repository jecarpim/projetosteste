
public class PessoaJuridica extends Pessoa{

	private String CGC; 
	private double TaxaIncentivo;
	
	public PessoaJuridica(String nome, int ano, String cgc) {
		super(nome, ano);
		CGC=cgc;
	}

	
	
	public double getTaxaIncentivo() {
		return TaxaIncentivo;
	}



	public void setTaxaIncentivo(double taxaIncentivo) {
		TaxaIncentivo = taxaIncentivo;
	}



	public String getCGC() {
		return CGC;
	}



	@Override
	public double calcBonus(int ano) {
		double bonus=0;
	
		bonus = (TaxaIncentivo * getTotalCompras()) * (ano - getAnoInscricao());
		return bonus;
	}

}
