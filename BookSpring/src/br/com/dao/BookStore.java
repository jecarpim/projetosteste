package br.com.dao;

import br.com.domain.classes.Book;

public interface BookStore {

		void saveOrUpdate(Book book); 
		void delete(Book book);
		Book findById(Long id); 
}
