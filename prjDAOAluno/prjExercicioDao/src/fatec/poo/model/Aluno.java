package fatec.poo.model;

public class Aluno {
	private String Ra;
	private String Nome;
	private String DataNasc;
	private String Sexo;
		
	public Aluno(String Ra, String Nome){
		this.Ra = Ra;
		this.Nome = Nome;
	}
	
	public String getNome(){
		return(Nome);
	}
	
	public String getRA(){
		return(Ra);
	}
	
	public void setDataNasc(String dtNasc){
		DataNasc = dtNasc;	
	}

	public void setSexo(String s){
		Sexo = s;	
	}	
}
