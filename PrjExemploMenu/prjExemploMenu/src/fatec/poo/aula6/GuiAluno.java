package fatec.poo.aula6;

import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.awt.Dimension;
import javax.swing.JLabel;
import java.awt.Rectangle;
import java.text.DecimalFormat;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.SoftBevelBorder;

public class GuiAluno extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JLabel lblRA = null;
	private JLabel jLabel = null;
	private JLabel jLabel1 = null;
	private JLabel jLabel2 = null;
	private JLabel jLabel3 = null;
	private JTextField txtRA = null;
	private JTextField txtNota1 = null;
	private JTextField txtNota2 = null;
	private JTextField txtNota3 = null;
	private JTextField txtNota4 = null;
	private JButton btnCalcular = null;
	private JButton btnLimpar = null;
	private JLabel jLabel4 = null;
	private JLabel jLabel5 = null;
	private JLabel lblMedia = null;
	private JLabel lblSituacao = null;
	private JButton btnSair = null;

	/**
	 * This method initializes txtRA	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getTxtRA() {
		if (txtRA == null) {
			txtRA = new JTextField();
			txtRA.setBounds(new Rectangle(132, 32, 113, 22));
		}
		return txtRA;
	}

	/**
	 * This method initializes txtNota1	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getTxtNota1() {
		if (txtNota1 == null) {
			txtNota1 = new JTextField();
			txtNota1.setBounds(new Rectangle(132, 63, 113, 22));
		}
		return txtNota1;
	}

	/**
	 * This method initializes txtNota2	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getTxtNota2() {
		if (txtNota2 == null) {
			txtNota2 = new JTextField();
			txtNota2.setBounds(new Rectangle(132, 96, 113, 22));
		}
		return txtNota2;
	}

	/**
	 * This method initializes txtNota3	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getTxtNota3() {
		if (txtNota3 == null) {
			txtNota3 = new JTextField();
			txtNota3.setBounds(new Rectangle(132, 125, 113, 22));
		}
		return txtNota3;
	}

	/**
	 * This method initializes txtNota4	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getTxtNota4() {
		if (txtNota4 == null) {
			txtNota4 = new JTextField();
			txtNota4.setBounds(new Rectangle(132, 155, 113, 22));
		}
		return txtNota4;
	}

	/**
	 * This method initializes btnCalcular	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnCalcular() {
		if (btnCalcular == null) {
			btnCalcular = new JButton();
			btnCalcular.setText("Calcular");
			btnCalcular.setBounds(new Rectangle(28, 255, 101, 33));
			btnCalcular.setName("Calcular");
			btnCalcular.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					DecimalFormat Formato = new DecimalFormat("0.0");
					double nota1, nota2, nota3, nota4, media;
					
					nota1 = Double.parseDouble(txtNota1.getText());
					nota2 = Double.parseDouble(txtNota2.getText());
					nota3 = Double.parseDouble(txtNota3.getText());
					nota4 = Double.parseDouble(txtNota4.getText());
					
					media = (nota1+ nota2+ nota3+ nota4)/4;
					
					lblMedia.setText(Formato.format(media));
					if (media>=5)
						lblSituacao.setText("Aprovado!");
					else
						lblSituacao.setText("Reprovado!");
					
					btnCalcular.setEnabled(false);
					btnLimpar.setEnabled(true);
				}
			});
		}
		return btnCalcular;
	}

	/**
	 * This method initializes btnLimpar	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnLimpar() {
		if (btnLimpar == null) {
			btnLimpar = new JButton();
			btnLimpar.setText("Limpar");
			btnLimpar.setEnabled(false);
			btnLimpar.setBounds(new Rectangle(165, 255, 87, 33));
			btnLimpar.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
				txtRA.setText("");	
				txtNota1.setText("");
				txtNota2.setText("");
				txtNota3.setText("");
				txtNota4.setText("");
				lblMedia.setText("");
				lblSituacao.setText("");
				btnCalcular.setEnabled(true);
				btnLimpar.setEnabled(false);
				
				txtRA.requestFocus();
				
					
				}
			});
		
		}
		return btnLimpar;
	}

	/**
	 * This method initializes btnSair	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnSair() {
		if (btnSair == null) {
			btnSair = new JButton();
			btnSair.setBounds(new Rectangle(273, 255, 95, 33));
			btnSair.setText("Sair");
			btnSair.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					dispose();
				}
			});
		}
		return btnSair;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Stub de m�todo gerado automaticamente
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				GuiAluno thisClass = new GuiAluno();
				thisClass.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				thisClass.setVisible(true);
			}
		});
	}

	/**
	 * This is the default constructor
	 */
	public GuiAluno() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(395, 339);
		this.setContentPane(getJContentPane());
		this.setTitle("Gui Aluno");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			lblSituacao = new JLabel();
			lblSituacao.setBorder(new SoftBevelBorder(SoftBevelBorder.LOWERED));
			lblSituacao.setBounds(new Rectangle(132, 217, 113, 22));
			lblSituacao.setText("");
			lblMedia = new JLabel();
			lblMedia.setBorder(new SoftBevelBorder(SoftBevelBorder.LOWERED));
			lblMedia.setBounds(new Rectangle(132, 188, 113, 22));
			lblMedia.setText("");
			jLabel5 = new JLabel();
			jLabel5.setText("Situa��o:");
			jLabel5.setBounds(new Rectangle(32, 221, 78, 16));
			jLabel4 = new JLabel();
			jLabel4.setText("M�dia: ");
			jLabel4.setBounds(new Rectangle(33, 187, 79, 16));
			jLabel3 = new JLabel();
			jLabel3.setText("4a Nota:");
			jLabel3.setBounds(new Rectangle(32, 154, 82, 16));
			jLabel2 = new JLabel();
			jLabel2.setText("3a Nota: ");
			jLabel2.setBounds(new Rectangle(34, 125, 72, 16));
			jLabel1 = new JLabel();
			jLabel1.setText("2a Nota");
			jLabel1.setBounds(new Rectangle(33, 95, 83, 16));
			jLabel = new JLabel();
			jLabel.setText("1a Nota: ");
			jLabel.setBounds(new Rectangle(32, 63, 85, 18));
			lblRA = new JLabel();
			lblRA.setText("R.A. do Aluno: ");
			lblRA.setBounds(new Rectangle(31, 30, 83, 21));
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(lblRA, null);
			jContentPane.add(jLabel, null);
			jContentPane.add(jLabel1, null);
			jContentPane.add(jLabel2, null);
			jContentPane.add(jLabel3, null);
			jContentPane.add(getTxtRA(), null);
			jContentPane.add(getTxtNota1(), null);
			jContentPane.add(getTxtNota2(), null);
			jContentPane.add(getTxtNota3(), null);
			jContentPane.add(getTxtNota4(), null);
			jContentPane.add(getBtnCalcular(), null);
			jContentPane.add(getBtnLimpar(), null);
			jContentPane.add(jLabel4, null);
			jContentPane.add(jLabel5, null);
			jContentPane.add(lblMedia, null);
			jContentPane.add(lblSituacao, null);
			jContentPane.add(getBtnSair(), null);
	
		}
		return jContentPane;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
