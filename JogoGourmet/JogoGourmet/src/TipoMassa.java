
public enum TipoMassa {
	LASSANHA("lasanha");
	
	private String descricao;
	
	TipoMassa(String descricao){
		this.descricao = descricao;
	}
	public String getDescricao() {
		return descricao;
	}
}
