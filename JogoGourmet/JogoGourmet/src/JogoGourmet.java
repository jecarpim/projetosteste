import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

public class JogoGourmet {

	public static void main(String[] args) {

		List<Prato> pratos = new ArrayList<Prato>();
		JOptionPane.showMessageDialog(null, "Pense em um prato que gosta.");
		Prato p = new Prato.PratoBuilder().tipoPrato(TipoPrato.MASSA).descricao(TipoMassa.LASSANHA.getDescricao())
				.escolhido(false).criarPrato();
		Prato p2 = new Prato.PratoBuilder().tipoPrato(TipoPrato.SOBREMESA).escolhido(false).criarPrato();
		pratos.add(p);
		pratos.add(p2);
		Boolean acertei = false;
		String pratoJogador = "";
		for (Prato prato : pratos) {
			if (Integer.valueOf(0)
					.equals(JOptionPane.showConfirmDialog(null,
							"O prato que voc� pensou � " + prato.getTipoPrato().getDescricao(), " ",
							JOptionPane.YES_NO_OPTION))) {
				if (prato.getDescricao().isEmpty()) {
					prato.getTipoPrato().setEscolhido(true);
				} else {
					if (Integer.valueOf(0).equals(JOptionPane.showConfirmDialog(null,
							"O prato que voc� pensou � " + prato.getDescricao(), " ", JOptionPane.YES_NO_OPTION))) {
						prato.setEscolhido(true);
					}
				}
			}
			if (prato.getTipoPrato().getEscolhido() || prato.getEscolhido()) {
				JOptionPane.showMessageDialog(null, "Acertei outra vez :D.");
				acertei = true;
				break;
			}
		}
		if(!acertei) {
			pratoJogador = JOptionPane.showInputDialog(null, " Em qual prato voc� estava pensando? ");
			 JOptionPane.showInputDialog(null, pratoJogador + " � ___ mas " + p2.getTipoPrato().getDescricao()+" n�o. ");
		}

	}

}
