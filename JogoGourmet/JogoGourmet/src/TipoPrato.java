
public enum TipoPrato {
	MASSA("massa",false),
	SOBREMESA("bolo de chocolate",false);

	private String descricao;
    private Boolean escolhido;
	
	public Boolean getEscolhido() {
		return escolhido;
	}

	public void setEscolhido(Boolean escolhido) {
		this.escolhido = escolhido;
	}

	TipoPrato(String descricao,Boolean escolhido) {
		this.descricao = descricao;
		this.escolhido = escolhido;
	}

	public String getDescricao() {
		return descricao;
	}

}
