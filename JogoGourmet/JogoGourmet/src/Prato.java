
public class Prato {
	private TipoPrato tipoPrato;

	private String descricao;

	private Boolean escolhido;

	public Boolean getEscolhido() {
		return escolhido;
	}

	public void setEscolhido(Boolean escolhido) {
		this.escolhido = escolhido;
	}

	public TipoPrato getTipoPrato() {
		return tipoPrato;
	}

	private Prato(TipoPrato tipoPrato,
			String descricao,
			Boolean escolhido) {
		this.descricao = descricao;
		this.tipoPrato = tipoPrato;
		this.escolhido = escolhido;
	}

	public Prato() {
		//super();
	}
	public static class PratoBuilder {

		private TipoPrato tipoPrato;
		private String descricao;
		private Boolean escolhido;

		public PratoBuilder() {

		}

		PratoBuilder tipoPrato(TipoPrato tipoPrato) {
			this.tipoPrato = tipoPrato;
			return this;
		}

		PratoBuilder descricao(String descricao) {
			this.descricao = descricao;
			return this;
		}

		PratoBuilder escolhido(Boolean escolhido) {
			this.escolhido = escolhido;
			return this;
		}
       public Prato criarPrato() {
    	   return new Prato(tipoPrato, descricao, escolhido);
       }
	}
	public void setPrato(TipoPrato tp) {
		this.tipoPrato = tp;
	}

	public String getDescricao() {
		return descricao;
	}

	
}
